//////////////////////////////////////////////////////
//  droneArucoEyeROSModule.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Jan 15, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////


#ifndef _DRONE_ARUCO_EYE_ROS_MODULE_H
#define _DRONE_ARUCO_EYE_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//String
//std::string, std::getline()
#include <string>

//Vector
//std::vector
#include <vector>



//Opencv
#include <opencv2/opencv.hpp>


#include "aruco_eye_ros/aruco_eye_ros_detector.h"


//ROS
#include "ros/ros.h"


//Drone Module
#include "droneModuleROS.h"

//Drone Msgs
#include "droneMsgsROS/obsVector.h"


//ROS Images
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>


// TODO REMOVE THIS DEPENDENCY
#include "cvg_utils_library.h"


#include "referenceFrames.h"

#include "nodes_definition.h"




//#define VERBOSE_DRONE_ARUCO_EYE_ROS_MODULE



/////////////////////////////////////////
// Class homogeneusTransformation
//
//   Description
//
/////////////////////////////////////////
class homogeneusTransformation
{
public:
    //Mat homog <-> Tvec & RVec
    static int createMatHomogFromVecs(cv::Mat& MatHomog, cv::Mat TransVec, cv::Mat RotVec);
    static int calculateVecsFromMatHomog(cv::Mat& TransVec, cv::Mat& RotVec, cv::Mat MatHomog);

    //Otros
    static int calculateTraVecYPRFromMatHomog(cv::Mat *YPRVec, cv::Mat *TraVec, cv::Mat MatHomog);
    static int calculateMatHomogFromYPRVecTraVec(cv::Mat* MatHomog, cv::Mat YPRVec, cv::Mat TraVec);
    static int calculateMatHomogFromRotMatTraVec(cv::Mat* MatHomog, cv::Mat RotMat, cv::Mat TraVec);

};




/////////////////////////////////////////
// Class droneArucoEyeROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneArucoEyeROSModule : public DroneModule, public virtual ArucoEyeROS
{
    // Config files
protected:
    std::string droneConfigurationFile;
    int cameraNumber;

    //Position of the camera in the drone
protected:
    cv::Mat matHomog_aruco_GMR_wrt_aruco_ALF;
    cv::Mat matHomog_drone_ALF_wrt_drone_GMR;
protected:
    int setCameraInTheDrone(double x, double y, double z, double yaw, double pitch, double roll);
    int setCameraInTheDrone(std::string droneConfigurationFile, int idCamera=0);


    // Image callback
protected:
    void imageCallback(const sensor_msgs::ImageConstPtr& msg);

    //Arucos detected
protected:
    std::string droneArucoListTopicName;
    ros::Publisher droneArucoListPubl; ////Publishers
    droneMsgsROS::obsVector droneArucoListMsg; //Messages
    bool publishArucoList();


    //Constructors and destructors
public:
    DroneArucoEyeROSModule(int argc,char **argv);
    ~DroneArucoEyeROSModule();

    //Init and close
public:
    void init();
    void close();


    //Run
public:
    int run_iter(ros::Time theTimeStamp);

    //Open
 public:
    void open();

protected:
    void readParameters();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    // Run
public:
    bool run();

};




#endif
