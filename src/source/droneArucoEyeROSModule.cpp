//////////////////////////////////////////////////////
//  droneArucoEyeROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Jan 15, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "drone_aruco_eye_ros_module/droneArucoEyeROSModule.h"



using namespace std;




////////////////////// homogeneusTransformation ////////////////////////////
int homogeneusTransformation::createMatHomogFromVecs(cv::Mat& MatHomog, cv::Mat TransVec, cv::Mat RotVec)
{
    //MatHomog.create(Size(4,4),CV_64F);
    MatHomog=cv::Mat::zeros(4,4,CV_32F);


    //
    cv::Mat RotMat;
    cv::Rodrigues(RotVec,RotMat);

    //cout<<RotMat<<endl;


    //Rotations
    /*
    float roll=RotVec.at<float>(0,0);
    Mat MatHRoll=Mat::zeros(4,4,CV_32F);
    MatHRoll.at<float>(0,0)=1.0;
    MatHRoll.at<float>(1,1)=cos(roll); MatHRoll.at<float>(1,2)=-sin(roll);
    MatHRoll.at<float>(2,1)=sin(roll); MatHRoll.at<float>(2,2)=cos(roll);
    MatHRoll.at<float>(3,3)=1.0;
    float pitch=RotVec.at<float>(1,0);
    Mat MatHPitch=Mat::zeros(4,4,CV_32F);
    MatHPitch.at<float>(0,0)=cos(pitch); MatHPitch.at<float>(0,2)=sin(pitch);
    MatHPitch.at<float>(1,1)=1.0;
    MatHPitch.at<float>(2,0)=-sin(pitch); MatHPitch.at<float>(2,2)=cos(pitch);
    MatHPitch.at<float>(3,3)=1.0;
    float yaw=RotVec.at<float>(2,0);
    Mat MatHYaw=Mat::zeros(4,4,CV_32F);
    MatHYaw.at<float>(0,0)=cos(yaw); MatHYaw.at<float>(0,1)=-sin(yaw);
    MatHYaw.at<float>(1,0)=sin(yaw); MatHYaw.at<float>(1,1)=cos(yaw);
    MatHYaw.at<float>(2,2)=1.0;
    MatHPitch.at<float>(3,3)=1.0;
    *MatHomog=MatHRoll*MatHPitch*MatHYaw;
    */
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            MatHomog.at<float>(i,j)=RotMat.at<float>(i,j);



    //Translation
    for(int i=0;i<3;i++)
    {
        MatHomog.at<float>(i,3)=TransVec.at<float>(i,0);
    }


    //Scale
    MatHomog.at<float>(3,3)=1.0;


    //cout<<*MatHomog<<endl;

    return 1;
}



int homogeneusTransformation::calculateVecsFromMatHomog(cv::Mat& TransVec, cv::Mat& RotVec, cv::Mat MatHomog)
{

    //Translation
    TransVec=cv::Mat::zeros(3,1,CV_32F);
    for(int i=0;i<3;i++)
    {
        TransVec.at<float>(i,0)=MatHomog.at<float>(i,3);
    }


    //Rotations
    RotVec=cv::Mat::zeros(3,1,CV_32F);



    //Rotacion
    cv::Mat RotMat=cv::Mat::zeros(3,3,CV_32F);
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            RotMat.at<float>(i,j)=MatHomog.at<float>(i,j);

    //cv::Mat AuxRotVec=*RotVec;
    cv::Rodrigues(RotMat,RotVec);


    return 1;
}



int homogeneusTransformation::calculateTraVecYPRFromMatHomog(cv::Mat *YPRVec, cv::Mat *TraVec, cv::Mat MatHomog)
{
    //Translation
    *TraVec=cv::Mat::zeros(3,1,CV_32F);
    for(int i=0;i<3;i++)
    {
        TraVec->at<float>(i,0)=MatHomog.at<float>(i,3);
    }



    //Rotation
    *YPRVec=cv::Mat::zeros(3,1,CV_32F);



    float yaw, pitch, roll;


    /*
    ///http://www.euclideanspace.com/maths/geometry/rotations/euler/indexLocal.htm
    ///http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToEuler/index.htm

    pitch=asin(MatHomog.at<float>(1,0));
    if(MatHomog.at<float>(1,0)==1) //North pole
    {
        yaw=atan2(MatHomog.at<float>(0,2),MatHomog.at<float>(2,2));
        roll=0.0;
    }
    else if(MatHomog.at<float>(1,0)==-1) //South pole
    {
        yaw=atan2(MatHomog.at<float>(0,2),MatHomog.at<float>(2,2));
        roll=0.0;
    }
    else
    {
        yaw=atan2(-MatHomog.at<float>(1,2),MatHomog.at<float>(1,1));
        roll=atan2(-MatHomog.at<float>(2,0),MatHomog.at<float>(0,0));
    }


    //end
    YPRVec->at<float>(0,0)=yaw;
    YPRVec->at<float>(1,0)=pitch;
    YPRVec->at<float>(2,0)=roll;


    return 1;
    */





    /*
    ////http://www.mathworks.es/matlabcentral/fileexchange/24589-kinematics-toolbox/content/kinematics/screws/rpy.m
    //Pitch
    float tol=1e-4;
    if(pow(MatHomog.at<float>(0,0),2) + pow(MatHomog.at<float>(0,1),2) >=tol)
    {
        pitch=atan2(MatHomog.at<float>(0,2), sqrt(pow(MatHomog.at<float>(0,0),2) + pow(MatHomog.at<float>(0,1),2))); // mal condicionada si está cerca de pi/2 o -pi/2
    }
    else
    {
        cout<<"!Mal condicionada pitch\n";
        pitch=asin(MatHomog.at<float>(0,2));
    }


    //Yaw and Roll
    float tolPitch=0.0;
    if(pitch>=PI/2-tolPitch)
    {
        //North pole
        cout<<"!Mal condicionada pitch a\n";
        roll = 0;
        yaw = atan2(MatHomog.at<float>(0,1), MatHomog.at<float>(1,1));
    }
    else if(pitch<=-PI/2+tolPitch)
    {
        //South pole
        cout<<"!Mal condicionada pitch b\n";
        roll = 0;
        yaw = -atan2(MatHomog.at<float>(0,1), MatHomog.at<float>(1,1));
    }
    else
    {
        //yaw = atan2(MatHomog.at<float>(1,0)/cos(pitch), MatHomog.at<float>(0,0)/cos(pitch));
        yaw=atan2(-MatHomog.at<float>(0,1),MatHomog.at<float>(0,0));
        //roll = atan2(MatHomog.at<float>(2,1)/cos(pitch), MatHomog.at<float>(2,2)/cos(pitch));
        roll=atan2(-MatHomog.at<float>(1,2),MatHomog.at<float>(2,2));
    }


    //
    YPRVec->at<float>(0,0)=yaw;
    YPRVec->at<float>(1,0)=pitch;
    YPRVec->at<float>(2,0)=roll;


    return 1;
    */



    ///http://en.wikibooks.org/wiki/Robotics_Kinematics_and_Dynamics/Description_of_Position_and_Orientation
    float tol=1e-4;

    //Pitch
    if(pow(MatHomog.at<float>(0,0),2) + pow(MatHomog.at<float>(0,1),2) >=tol)
    {
      float my_sign = MatHomog.at<float>(0,0) > 0 ? 1.0 : -1.0;
      pitch=atan2(MatHomog.at<float>(0,2), my_sign*sqrt(pow(MatHomog.at<float>(0,0),2) + pow(MatHomog.at<float>(0,1),2))); // mal condicionada si está cerca de pi/2 o -pi/2


    //	if (MatHomog.at<float>(0,2) < 0 && MatHomog.at<float>(0,2) < 0 && MatHomog.at<float>(0,1))

      /*
    cout << "YPR Calculation ........... GOOD Pitch!!!!!!";
    cout <<  " 0,2= " << MatHomog.at<float>(0,2);
    cout <<  " 0,0= " << MatHomog.at<float>(0,0);
    cout <<  " 0,1= " << MatHomog.at<float>(0,1) << endl;
      */
    }
    else
    {
        //TODO
      cout<<"!Bad conditioning in Pitch. Pitch =+-pi/2" << endl;
      pitch = cvg_utils_library::asin_ws(MatHomog.at<float>(0,2));
    }



    float tol2=-1e-1;

    //Roll =f(pitch) -> pitch=+-pi/2 || yaw=+-pi/2
    if(abs(MatHomog.at<float>(2,2))<=tol2)
    {
      cout<<"!Bad conditioning in Roll. Roll =+-pi/2" << endl;
        //TODO
        roll=0.0;
    }
    else
    {
        roll=atan2(-MatHomog.at<float>(1,2),MatHomog.at<float>(2,2));
    }


    //Yaw =f(roll)
    if(abs(MatHomog.at<float>(0,0))<=tol2)
    {
      cout<<"!Bad conditioning in Yaw. Yaw =+-pi/2" << endl;
        //TODO
      float aux = (cos(roll)-MatHomog.at<float>(1,1)/MatHomog.at<float>(2,1)*sin(roll)) / (MatHomog.at<float>(1,0)-MatHomog.at<float>(1,1)*MatHomog.at<float>(2,0)/MatHomog.at<float>(2,1));
      yaw = cvg_utils_library::asin_ws(aux);
    }
    else
    {
        yaw=atan2(-MatHomog.at<float>(0,1),MatHomog.at<float>(0,0));
    }


    //end
    YPRVec->at<float>(0,0)=yaw;
    YPRVec->at<float>(1,0)=pitch;
    YPRVec->at<float>(2,0)=roll;


    return 1;

}


int homogeneusTransformation::calculateMatHomogFromYPRVecTraVec(cv::Mat* MatHomog, cv::Mat YPRVec, cv::Mat TraVec)
{
    *MatHomog=cv::Mat::zeros(4,4,CV_32F);


    //Rotations

    float roll=YPRVec.at<float>(2,0);
    cv::Mat MatHRoll=cv::Mat::zeros(4,4,CV_32F);
    MatHRoll.at<float>(0,0)=1.0;
    MatHRoll.at<float>(1,1)=cos(roll); MatHRoll.at<float>(1,2)=-sin(roll);
    MatHRoll.at<float>(2,1)=sin(roll); MatHRoll.at<float>(2,2)=cos(roll);
    MatHRoll.at<float>(3,3)=1.0;
    float pitch=YPRVec.at<float>(1,0);
    cv::Mat MatHPitch=cv::Mat::zeros(4,4,CV_32F);
    MatHPitch.at<float>(0,0)=cos(pitch); MatHPitch.at<float>(0,2)=sin(pitch);
    MatHPitch.at<float>(1,1)=1.0;
    MatHPitch.at<float>(2,0)=-sin(pitch); MatHPitch.at<float>(2,2)=cos(pitch);
    MatHPitch.at<float>(3,3)=1.0;
    float yaw=YPRVec.at<float>(0,0);
    cv::Mat MatHYaw=cv::Mat::zeros(4,4,CV_32F);
    MatHYaw.at<float>(0,0)=cos(yaw); MatHYaw.at<float>(0,1)=-sin(yaw);
    MatHYaw.at<float>(1,0)=sin(yaw); MatHYaw.at<float>(1,1)=cos(yaw);
    MatHYaw.at<float>(2,2)=1.0;
    MatHYaw.at<float>(3,3)=1.0;
    *MatHomog=MatHRoll*MatHPitch*MatHYaw;

    /*
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            MatHomog->at<float>(i,j)=RotMat.at<float>(i,j);
    */


    //Translation
    for(int i=0;i<3;i++)
    {
        MatHomog->at<float>(i,3)=TraVec.at<float>(i,0);
    }


    //Scale
    MatHomog->at<float>(3,3)=1.0;


    return 1;
}



int homogeneusTransformation::calculateMatHomogFromRotMatTraVec(cv::Mat* MatHomog, cv::Mat RotMat, cv::Mat TraVec)
{
    *MatHomog=cv::Mat::zeros(4,4,CV_32F);

    //Rotation
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            MatHomog->at<float>(i,j)=RotMat.at<float>(i,j);


    //Translation
    for(int i=0;i<3;i++)
    {
        MatHomog->at<float>(i,3)=TraVec.at<float>(i,0);
    }


    //Scale
    MatHomog->at<float>(3,3)=1.0;

    return 1;
}










////////// Drone Aruco Eye ///////////
DroneArucoEyeROSModule::DroneArucoEyeROSModule(int argc, char **argv):
    DroneModule(),
    ArucoEyeROS(argc, argv)
{
    init();
    return;
}


DroneArucoEyeROSModule::~DroneArucoEyeROSModule()
{
    close();
    return;
}

void DroneArucoEyeROSModule::init()
{
    ArucoEyeROS::init();


    // matHomog_aruco_GMR_wrt_aruco_ALF
    matHomog_aruco_GMR_wrt_aruco_ALF = cv::Mat::eye(4,4,CV_32F);

    // assign value to matHomog_aruco_GMR_wrt_aruco_ALF
    double x = 0.0, y = 0.0, z = 0.0;
    double yaw    = (M_PI/180.0)*(+90.0); // rad
    double pitch  = 0.0;
    double roll   = (M_PI/180.0)*(-90.0); // rad
    referenceFrames::createHomogMatrix_wYvPuR( &matHomog_aruco_GMR_wrt_aruco_ALF, x, y, z, yaw, pitch, roll);

    // End
    return;
}


void DroneArucoEyeROSModule::close()
{
    ArucoEyeROS::close();

    return;
}

void DroneArucoEyeROSModule::readParameters()
{
    // Config files
    //
    ros::param::param<std::string>("~drone_configuration_file", droneConfigurationFile,"droneConfiguration.xml");
    std::cout<<"droneConfigurationFile="<<droneConfigurationFile<<std::endl;

    // Parameters
    //
    ros::param::param<int>("~camera_number", cameraNumber, 0);
    std::cout<<"camera_number="<<cameraNumber<<std::endl;

    // Topic names
    //
    ros::param::param<std::string>("~drone_aruco_list_topic_name", droneArucoListTopicName, "arucoObservation");
    std::cout<<"droneArucoListTopicName="<<droneArucoListTopicName<<std::endl;

    return;
}

void DroneArucoEyeROSModule::open()
{
    // Node Handle
    ros::NodeHandle n;

    //Node
    DroneModule::open(n);

    // Open of ArucoEyeROS
    ArucoEyeROS::open();

    // Read parameters of this one
    readParameters();


    //Configure camera in the drone
    if(!setCameraInTheDrone(droneConfigurationFile, cameraNumber))
    {
#ifdef VERBOSE_DRONE_ARUCO_EYE_ROS_MODULE
        cout<<"[DAE-ROS] Error setting camera in the drone!!!!"<<endl;
#endif
        return;
    }


    // Open ROS for old topics

    //Publisher aruco 3D pose in the old way
    droneArucoListPubl = n.advertise<droneMsgsROS::obsVector>(droneArucoListTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //Autostart the module
    //moduleStarted=true; //JL to remove!

    //End
    return;
}


int DroneArucoEyeROSModule::setCameraInTheDrone(double x, double y, double z, double yaw, double pitch, double roll)
{
    //Set camera parameters
    matHomog_drone_ALF_wrt_drone_GMR = cv::Mat::eye(4,4,CV_32F);
    referenceFrames::createHomogMatrix_wYvPuR( &matHomog_drone_ALF_wrt_drone_GMR, x, y, z, yaw, pitch, roll);

    return 1;
}


int DroneArucoEyeROSModule::setCameraInTheDrone(std::string droneConfigurationFile, int idCamera)
{

    double x, y, z, yaw, pitch, roll;
    bool cameraFound=false;

    //Jesus method. Temporary removed, because it cannot handle multiple cameras. TODO!
//    try
//    {
//        XMLFileReader my_xml_reader(droneConfigurationFile);

//        int idCamera_fromfile = my_xml_reader.readIntValue( {"droneConfiguration","camera","id"} );
//        if(idCamera_fromfile == idCamera)
//            cameraFound=true;

//        x = my_xml_reader.readDoubleValue( {"droneConfiguration","camera","position","x"} );
//        y = my_xml_reader.readDoubleValue( {"droneConfiguration","camera","position","y"} );
//        z = my_xml_reader.readDoubleValue( {"droneConfiguration","camera","position","z"} );
//        yaw   = (M_PI/180.0)*my_xml_reader.readDoubleValue( {"droneConfiguration","camera","position","yaw"} );
//        pitch = (M_PI/180.0)*my_xml_reader.readDoubleValue( {"droneConfiguration","camera","position","pitch"} );
//        roll  = (M_PI/180.0)*my_xml_reader.readDoubleValue( {"droneConfiguration","camera","position","roll"} );
//    }
//    catch ( cvg_XMLFileReader_exception &e)
//    {
//        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
//    }

    //JL Method. Unsafe.
    //read xml
    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(droneConfigurationFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
#ifdef VERBOSE_DRONE_ARUCO_EYE
        cout<<"[DAE] I cannot open xml file: "<<droneConfigurationFile<<endl;
#endif
        return 0;
    }

    ///droneConfiguration
    pugi::xml_node droneConfiguration = doc.child("droneConfiguration");

    //vars
    std::string readingValue;

    //search for the camera
    for(pugi::xml_node camera = droneConfiguration.child("camera");camera; camera = camera.next_sibling("camera"))
    {
        int id;
        readingValue=camera.child_value("id");
        istringstream convertid(readingValue);
        convertid>>id;

        if(id!=idCamera)
            continue;
        else
           {
            cameraFound=true;

            //read camera parameters
            readingValue=camera.child("position").child_value("x");
            istringstream convertx(readingValue);
            convertx>>x;

            readingValue=camera.child("position").child_value("y");
            istringstream converty(readingValue);
            converty>>y;

            readingValue=camera.child("position").child_value("z");
            istringstream convertz(readingValue);
            convertz>>z;

            readingValue=camera.child("position").child_value("yaw");
            istringstream convertyaw(readingValue);
            convertyaw>>yaw;
            yaw   *= (M_PI/180.0);

            readingValue=camera.child("position").child_value("pitch");
            istringstream convertpitch(readingValue);
            convertpitch>>pitch;
            pitch *= (M_PI/180.0);

            readingValue=camera.child("position").child_value("roll");
            istringstream convertroll(readingValue);
            convertroll>>roll;
            roll  *= (M_PI/180.0);

            break;
        }

    }

    if(!cameraFound)
        return 0;

    std::cout << "DroneArucoEye::setCameraInTheDrone" << std::endl;
    std::cout << "x:" << x << std::endl;
    std::cout << "y:" << y << std::endl;
    std::cout << "z:" << z << std::endl;
    std::cout << "yaw:" <<   yaw*(180.0/M_PI) << std::endl;
    std::cout << "pitch:" << pitch*(180.0/M_PI) << std::endl;
    std::cout << "roll:" <<  roll*(180.0/M_PI) << std::endl;

    //Set camera parameters
    if(!setCameraInTheDrone(x,y,z,yaw,pitch,roll))
        return 0;

    return 1;
}


void DroneArucoEyeROSModule::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    // Check functionalities of drone module
    if(!moduleStarted)
        return;

    // Callback of the ArucoEyeROS
    ArucoEyeROS::imageCallback(msg);

    // Publish in the old format, doing the conversion to drone center frame
    run_iter(msg->header.stamp);

    // End
    return;
}



//Reset
bool DroneArucoEyeROSModule::resetValues()
{
    if(!DroneModule::resetValues())
        return false;


    return true;
}

//Start
bool DroneArucoEyeROSModule::startVal()
{
    if(!DroneModule::startVal())
        return false;


    return true;
}

//Stop
bool DroneArucoEyeROSModule::stopVal()
{
    if(!DroneModule::stopVal())
        return false;

    return true;
}


//Run
int DroneArucoEyeROSModule::run_iter(ros::Time theTimeStamp)
{

    //Prepare message
    droneArucoListMsg.obs.clear();
    //Time
    droneArucoListMsg.time=theTimeStamp.toSec();
    droneArucoListMsg.YPR_system      = "wYvPuR";
    droneArucoListMsg.target_frame    = "aruco_GMR";
    droneArucoListMsg.reference_frame = "drone_GMR";


    //Get codes reconstructed
    std::vector<ArucoMarker> TheMarkers;
    MyArucoEye.getMarkersList(TheMarkers);

    // Iterate over the markers to fill the messages
    for(unsigned int i=0; i<TheMarkers.size(); i++)
    {

        // Detected
        aruco::Marker TheArucoMarker=TheMarkers[i].getMarker();


        // Reconstructed
        if(TheMarkers[i].is3DReconstructed())
        {
            // reference frame related matrixes, see documentation/IMAV_Frames/IMAV13_Frames_documentation.pdf
            cv::Mat matHomog_aruco_GMR_wrt_drone_GMR(4,4,CV_32F);       // as required by ArUCo SLAM module
            cv::Mat matHomog_aruco_ALF_wrt_drone_ALF(4,4,CV_32F);       // as returned by the ArUCo library

            //Transform to center of gravity of the drone
            homogeneusTransformation::createMatHomogFromVecs( matHomog_aruco_ALF_wrt_drone_ALF, TheArucoMarker.Tvec, TheArucoMarker.Rvec);

            matHomog_aruco_GMR_wrt_drone_GMR = matHomog_drone_ALF_wrt_drone_GMR*
                                               matHomog_aruco_ALF_wrt_drone_ALF*
                                               matHomog_aruco_GMR_wrt_aruco_ALF;

            double x = 0.0, y = 0.0, z = 0.0, yaw = 0.0, pitch = 0.0, roll = 0.0;
            referenceFrames::getxyzYPRfromHomogMatrix_wYvPuR( matHomog_aruco_GMR_wrt_drone_GMR, &x, &y, &z, &yaw, &pitch, &roll);

    #ifdef VERBOSE_DRONE_ARUCO_EYE_ROS_MODULE
            cout<<"[DAE-ROS] Marker id="<<TheArucoMarker.id<<"; HomogTransInWorld="<<matHomog_aruco_GMR_wrt_drone_GMR<<endl;

            std::cout<<"[DAE-ROS]  Homog_aruco_GMR_wrt_drone_GMR_wYvPuR =\n"<<
                       "    x = "   << x   << " y = "     << y     << " z = "   << z    << endl <<
                       "    yaw = " << yaw*(180.0/M_PI) << " pitch = " << pitch*(180.0/M_PI) << " roll = "<< roll*(180.0/M_PI) << endl;
    #endif

            //message
            droneMsgsROS::Observation3D TheObservation3D;


            // Fill the message
            TheObservation3D.id = TheArucoMarker.id;
            TheObservation3D.x = x;
            TheObservation3D.y = y;
            TheObservation3D.z = z;
            TheObservation3D.yaw   = yaw;
            TheObservation3D.pitch = pitch;
            TheObservation3D.roll  = roll;

            // Push
            droneArucoListMsg.obs.push_back(TheObservation3D);
        }

    }


    //Publish
    if(publishArucoList())
        return 1;


    //end
    return 0;
}



bool DroneArucoEyeROSModule::publishArucoList()
{
    if(droneModuleOpened==false)
        return 0;

    //publish
    droneArucoListPubl.publish(droneArucoListMsg);

    //end
    return 0;
}


bool DroneArucoEyeROSModule::run()
{
    ArucoEyeROS::run();

    return true;
}

